#![no_std]
#![no_main]

use core::str;

use panic_halt as _;

const BAUDRATE : u32 = 57600;

const GET_BAUDRATE : u8 = b'b';

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut serial = arduino_hal::default_serial!(dp, pins, BAUDRATE);

    ufmt::uwriteln!(&mut serial, "Hello from Arduino!\r").unwrap();

    let mut buffer : [u8; 256] = [0; 256];
    let mut pos;
    loop {
        pos = 0;
        loop {
            let b = serial.read_byte();
            if b == b'\n' {
                break;
            }
            else {
                buffer[pos] = b;
                pos += 1
            }
        }

        let line = match str::from_utf8(&buffer[..pos]) {
            Ok(s) => s,
            Err(_) => "error parsing input as utf8",
        };
        match buffer[0] {
            GET_BAUDRATE => ufmt::uwriteln!(&mut serial, "Baud={}\r", line).unwrap(),
            b'\n' => (),
            _ => ufmt::uwriteln!(&mut serial, "Got {}!\r", line).unwrap(),
        }

        // Answer
        
    }
}

// #[arduino_hal::entry]
// fn main() -> ! {
//     let dp = arduino_hal::Peripherals::take().unwrap();
//     let pins = arduino_hal::pins!(dp);

//     /*
//      * For examples (and inspiration), head to
//      *
//      *     https://github.com/Rahix/avr-hal/tree/main/examples
//      *
//      * NOTE: Not all examples were ported to all boards!  There is a good chance though, that code
//      * for a different board can be adapted for yours.  The Arduino Uno currently has the most
//      * examples available.
//      */

//     let mut green = pins.d13.into_output();
//     // let mut blue = pins.d11.into_output();
//     let mut red = pins.d12.into_output();


//     // let mut timer = arduino_hal::simple_pwm::Timer2Pwm::new(dp.TC2, Prescaler::Prescale64);
//     // let mut blue = pins.d11.into_output().into_pwm(&mut timer);
//     // blue.set_duty(100);
//     // blue.enable();

//     let mut timer = arduino_hal::simple_pwm::Timer2Pwm::new(dp.TC2, Prescaler::Prescale64);
//     let mut blue = pins.d3.into_output().into_pwm(&mut timer);
//     blue.set_duty(100);
//     blue.enable();

//     loop {
//         green.set_high();
//         arduino_hal::delay_ms(1000);
//         green.set_low();

//         // blue.set_high();
//         blue.set_duty(255);
//         // for i in 0..20 {
//             // blue.set_duty(i * 10);
//             // arduino_hal::delay_ms(100)
//         // }
//         arduino_hal::delay_ms(1000);
//         blue.set_duty(0);
//         // blue.set_low();

//         red.set_high();
//         arduino_hal::delay_ms(1000);
//         red.set_low();

//     }
// }
